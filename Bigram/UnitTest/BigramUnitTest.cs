﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bigram;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace UnitTest
{
    [TestClass]
    public class BigramUnitTest
    {
        [TestMethod]
        public void FileExist()
        {
            bool exist = Helper.FileExist(@"C:\CNE\Bigram\Bigram.txt");
            Assert.AreEqual(true, exist);
        }

        [TestMethod]
        public void WrongFilePath()
        {
            bool exist = Helper.FileExist(@"C:\abc");
            Assert.AreEqual(false, exist);
        }

        [TestMethod]
        public void GenerateBigram()
        {
            string bgramText = "I found the best of the best";

            Bigram.Bigram bgram = new Bigram.Bigram();
            var list = bgram.ConvertToNGram(bgramText, 2).ToList();

            Assert.AreEqual(10, list.Count);
        }
    }
}
