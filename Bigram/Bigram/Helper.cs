﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Bigram
{
    public class Helper
    {
        #region FileExist
        /// <summary>
        /// This method will check whether the file exist or not
        /// </summary>
        /// <param name="fileLocation">Actual location of the file</param>
        /// <returns></returns>
        public static bool FileExist(string fileLocation)
        {
            return File.Exists(@fileLocation) ? true : false;
        }
        #endregion

        #region ProcessFile
        /// <summary>
        /// This method will Process the file and generate the Bigrams accordingly
        /// </summary>
        /// <param name="fileLocation">Actual Location of the file</param>
        public static void ProcessFile(string fileLocation)
        {
            string line = File.ReadAllText(@fileLocation);

            if (!string.IsNullOrEmpty(line))
            {
                Bigram bgram = new Bigram();
                var list = bgram.ConvertToNGram(line, 2).ToList();

                var q = from x in list
                        group x by x into g
                        let count = g.Count()
                        orderby count descending
                        select new { Value = g.Key, Count = count };

                foreach (var x in q)
                {
                    Console.WriteLine(x.Value + " - Count: " + x.Count);
                }

            }
        }
        #endregion

    }
}
