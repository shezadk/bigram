﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bigram
{
    public class Bigram
    {
        #region ConvertToNGram
        /// <summary>
        /// This Method will Convert the string to a bigram
        /// </summary>
        /// <param name="userProvidedString">Input we got from the file</param>
        /// <param name="nGramSize">its hardcoded 2, to generate bigram only</param>
        /// <returns>Collection of bigrams</returns>
        public IEnumerable<string> ConvertToNGram(string userProvidedString, int nGramSize)
        {
            if (nGramSize == 0) throw new Exception("nGram size not provided");

            StringBuilder strNGram = new StringBuilder();
            Queue<int> wordLengths = new Queue<int>();

            int wordCounter = 0;
            int lastWordLength = 0;

            //Check if the first character is valid or not if its valid then append the first character.
            lastWordLength = validateFirstCharacter(userProvidedString, strNGram, lastWordLength);

            //Let's generate the ngrams
            for (int i = 1; i < userProvidedString.Length - 1; i++)
            {
                char before = userProvidedString[i - 1];
                char after = userProvidedString[i + 1];

                //Take care of the punctuation surrounded by letters or numbers on both sides.
                if (char.IsLetterOrDigit(userProvidedString[i]) || (userProvidedString[i] != ' ' && (char.IsSeparator(userProvidedString[i]) || char.IsPunctuation(userProvidedString[i])) && (char.IsLetterOrDigit(before) && char.IsLetterOrDigit(after))))
                {
                    strNGram.Append(userProvidedString[i]);
                    lastWordLength++;
                }
                else
                {
                    if (lastWordLength > 0)
                    {
                        wordLengths.Enqueue(lastWordLength);
                        lastWordLength = 0;
                        wordCounter++;

                        if (wordCounter >= nGramSize)
                        {
                            yield return strNGram.ToString();
                            strNGram.Remove(0, wordLengths.Dequeue() + 1);
                            wordCounter -= 1;
                        }

                        strNGram.Append(" ");
                    }
                }
            }
            strNGram.Append(userProvidedString.Last());
            yield return strNGram.ToString();
        }
        #endregion

        #region validateFirstCharacter
        /// <summary>
        /// This method will validate the User provided Strings
        /// </summary>
        /// <param name="userProvidedString"></param>
        /// <param name="strNGram"></param>
        /// <param name="lastWordLength"></param>
        /// <returns></returns>
        private static int validateFirstCharacter(string userProvidedString, StringBuilder strNGram, int lastWordLength)
        {
            if (userProvidedString != "" && char.IsLetterOrDigit(userProvidedString[0]))
            {
                strNGram.Append(userProvidedString[0]);
                lastWordLength++;
            }
            return lastWordLength;
        }
        #endregion

    }
}
