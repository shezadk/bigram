﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Bigram
{
    public class Program
    {
        static void Main(string[] args)
        {
            Initialize();

            // Get File Location from the user
            string fileLocation = Console.ReadLine();

            try
            {
                if (Helper.FileExist(@fileLocation))
                    Helper.ProcessFile(fileLocation);
                else
                    RecursivelyCheckFileLocation(fileLocation);

                Console.ReadLine();
               
            }
            catch (IOException e)
            {
                // display error to user
                Console.WriteLine("Exception : ", e.Message);
            }
        }


        #region Initialize
        /// <summary>
        /// This is the helper method for Input Instructions to the User
        /// </summary>
        private static void Initialize()
        {
            Console.Clear();
            Console.WriteLine("Bigram Example");
            Console.WriteLine("");
            Console.WriteLine("Please provide the complete File Path (File Location + File Name with extension.");
            Console.WriteLine("Example C:\\Bigram\\Bigram.txt");
            Console.WriteLine("");
        }
        #endregion

        #region RecursivelyCheckFileLocation
        /// <summary>
        /// This method will recrusively check for the Correct File.
        /// </summary>
        /// <param name="fileLocation">Location of the File</param>
        private static void RecursivelyCheckFileLocation(string fileLocation)
        {
            while (!File.Exists(@fileLocation))
            {
                FurtherInstructions();

                fileLocation = Console.ReadLine();

                if (Helper.FileExist(fileLocation))
                    Helper.ProcessFile(fileLocation);
            }
        }

        /// <summary>
        /// Further Instructions to USers
        /// </summary>
        private static void FurtherInstructions()
        {
            Console.WriteLine("");
            Console.WriteLine("Please provide a valid File Path ");
            Console.WriteLine("");
        }
        #endregion 

        
    }
}
